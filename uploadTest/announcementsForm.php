<html>
<style>
  input, select {
    width: 100%;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
  }

  textarea{
    width: 100%;
    height: 100px;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
  }

  input[type=submit] {
    width: 100%;
    background-color: #4CAF50;
    color: white;
    padding: 14px 20px;
    margin: 8px 0;
    border: none;
    border-radius: 4px;
    cursor: pointer;
  }

  input[type=submit]:hover {
    background-color: #45a049;
  }

  div {
    width:50%;
    border-radius: 5px;
    background-color: #f2f2f2;
    padding: 20px;
}
</style>
<?php include 'navBar.php' ?>

<div>
  <h2>submit announcement test</h2>

  <form action="submit/announcementsSubmit.php" method="post">
    <label>Subject</label>
    <input name="subject" type="text" required ></input>
    </br>

    <label>comments</label>
    <textarea name="comments" type="textarea" required></textarea>
    </br>

    <label>Location</label>
    <input name="location" type="text" ></input>
    </br>

    <label>speeker</label>
    <input name="speaker" type="text" ></input>
    </br>

    <label>start date</label>
    <input name="startDate" type="date" required></input>
    </br>

    <label>enddate</label>
    <input name="endDate" type="date" required></input>
    </br>
    <input type="submit" value="upload"></input>
  </form>
</div>
</html>

<?php

?>
