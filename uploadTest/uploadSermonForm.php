<?php
  include 'navBar.php' ;
  include 'submit/DBFunctions.php';
?>

<style>
  input, select {
    width: 100%;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
  }

  input[type=textarea]{
    width: 100%;
    height: 100px;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
  }

  input[type=submit] {
    width: 100%;
    background-color: #4CAF50;
    color: white;
    padding: 14px 20px;
    margin: 8px 0;
    border: none;
    border-radius: 4px;
    cursor: pointer;
  }

  input[type=submit]:hover {
    background-color: #45a049;
  }

  div {
    width:50%;
    border-radius: 5px;
    background-color: #f2f2f2;
    padding: 20px;
}
</style>

<div>
  <h2>sermon Upload file</h2>

  <form action="submit/uploadSermon.php" method="post" enctype="multipart/form-data">

    <label>Date</label>
    <input name="sermonDate" type="date" required ></input>
    </br>

    <label>AM/PM</label>
    <select name="sermonTime" required>
      <option value="am">AM</option>
      <option value="pm">PM</option>
    </select>
    </br>

    <label>Title</label>
    <input name="sermonTitle" type="text" required ></input>
    </br>

    <label>Speaker</label>
    <input name="sermonSpeaker" type="text" required></input>
    </br>

    <label>Bible passage</label>
    <input name="sermonReading" type="text" required></input>
    </br>

    <label>file</label>
    <input name="fileUpload" id="fileUpload" type="file" required ></input>
    </br>

    <label>Series</label>
    <input name="sermonSeries" type="text" ></input>
    </br>

    <label>Series Number</label>
    <input name="sermonSeriesNo" type="number" value="0"></input>
    </br>

    <label>Tags (use commas between)</label>
    <input name="sermonTags" type="text" ></input>
    </br>

    <input type="submit" value="upload"></input>
  </form>
</div>
</html>
