<?php
  include 'DBFunctions.php';

  //echo "<p>conection to be made</p>";
  //$conn = getConnection();
  //echo "<p>conection made</p>";
  $sermonDate = test_input($_POST['sermonDate']);
  $sermonTime = test_input($_POST['sermonTime']);
  $sermonTitle = test_input($_POST['sermonTitle']);
  $sermonSpeaker = test_input($_POST['sermonSpeaker']);
  $sermonReading = test_input($_POST['sermonReading']);
  $sermonSeries = test_input($_POST['sermonSeries']);
  $sermonSeriesNo = test_input($_POST['sermonSeriesNo']);
  $sermonTags = test_input($_POST['sermonTags']);
  //$sermonTags = Array();
  //$sermonTags[] = explode("," , $_POST['sermonTags']);


  $fileName = $sermonDate . $sermonTime;

  $sermonFilePath = uploadToServer("sermonUploads/", $fileName);


  if(tableExists("sermonRec")==0){
    createsermonRecTable();
    //echo "table dosent exist";
  }

  uploadToDatabace(
    $sermonDate, $sermonTime, $sermonFilePath, $sermonTitle, $sermonSpeaker,
    $sermonReading, $sermonSeries, $sermonSeriesNo, $sermonTags );

  //Functions

  function uploadToDatabace(
    $sermonDate, $sermonTime, $sermonFilePath, $sermonTitle, $sermonSpeaker,
    $sermonReading, $sermonSeries, $sermonSeriesNo, $sermonTags ){
    $conn = getConnection();

    $sql = "INSERT INTO
      `sermonRec` (`sermonDate`, `sermonTime`, `sermonFilePath`, `sermonTitle`, `sermonSpeaker`, `sermonReading`, `sermonSeries`, `sermonSeriesNo`, `sermonTags`)
      VALUES ('$sermonDate', '$sermonTime', '$sermonFilePath', '$sermonTitle', '$sermonSpeaker', '$sermonReading', '$sermonSeries', '$sermonSeriesNo', '$sermonTags')";

    if (mysqli_query($conn, $sql)) {
      echo "New record created successfully";
    } else {
      echo "Error: " . $sql . "<br>" . mysqli_error($conn);
    }

    mysqli_close($conn);

  }

  function uploadToServer( $target_dir, $filePath){
    //$target_dir = "uploads/";


    //try https://www.tutorialspoint.com/php/php_file_uploading.htm
    $errorsArray = array();
    $file_name = $_FILES['fileUpload']['name'];
    $file_size = $_FILES['fileUpload']['size'];
    $file_temp = $_FILES['fileUpload']['tmp_name'];
    $file_type = $_FILES['fileUpload']['type'];

    //$errorsArray[] = $_FILES['fileUpload']['error'];

    $file_ext=strtolower(end(explode('.',$file_name)));
    //$file_ext = "mp3";
    //echo $file_ext . "</br>";


    $expensions= array('txt', 'mp3','mp4');

    if (in_array($file_ext, $expensions)==false){
      $errorsArray[]="extention not allowed, please chose a TXT, mp3 or mp4. file is:". $file_ext;
      echo $file_ext . "</br>";
      echo $file_name . "</br>";
    }

    if(!is_writable("../". $target_dir)){
      //echo "error in dir";
      $errorsArray[]="Dir is not Writable </br>";
    }

    if($file_size > 100000000) {
         $errors[]='File size must be excately 100 MB';
      }

    if (empty($errorsArray)== true){
      $returnFilePath = $target_dir. $filePath. ".". $file_ext;

      if(move_uploaded_file($file_temp, "../". $returnFilePath)){
        echo "Sucess:". $file_name . " was Uploded to the server</br>" ;
      }
      else{
        echo "Error: with move uploaded file function </br>";
        print_r($errorsArray);
        echo "</br>";
        print_r($file_temp);
      }
    }else{
      echo "errors Array has errors </br>";
      print_r($errorsArray);
    }

    return $returnFilePath;
  }

?>
