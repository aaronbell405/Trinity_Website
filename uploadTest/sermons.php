<html>
  <?php
    include 'navBar.php' ;
    include 'submit/DBFunctions.php';
  ?>
  <style>
    .sermon{
      margin: 10;
      width: 100%;
      background-color: rgb(124, 172, 235);
    }

    .metaInfo{
      display: flex;

    }

    .metainfo p{
      margin-right: 30;
    }
  </style>

  <h2>Sermons</h2>

  <?php
    //Connect to the server
    $conn = getConnection();

    /*query will need to be changed to reflect the lenth of artacals and the
    relavince of aticals.
    */
    $query = "SELECT * FROM `sermonRec`";

    $result = mysqli_query($conn, $query);

    while($row = mysqli_fetch_array($result)){
      echo(
        "<div class='sermon'>".
          "<h3 class='Title'>".
          $row['sermonTitle']. " (". $row['sermonDate']. " ". $row['sermonTime'].
          ")</h3>".
          "<p class='sermonFilePath'>".
          //$row['sermonFilePath'].
          "</p>".
          "<audio controls>
            <source src='' type='audio/ogg'>
            <source src='". $row['sermonFilePath'] ."' type='audio/mpeg'>
            Your browser does not support the audio tag.
            </audio>".
          "<div class='metaInfo'>".
            //"<p class='sermonTitle'> sermon". $row['sermonTitle']. "</p>".
            "<p class='sermonSpeaker'>Speaker: ". $row['sermonSpeaker']. "</p>".
            "<p class='sermonReading'>Reading: ". $row['sermonReading']. "</p>".
            "<p class='sermonSeries'> Series: ". $row['sermonSeries']. " - #". $row['sermonSeriesNo']. "</p> ".

          "</div>".
          "<p class='sermonTags'>". $row['sermonTags']. "</p>".
        "</div>"
      );
    }
  ?>

</html>
