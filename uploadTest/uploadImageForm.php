<?php
  include 'navBar.php' ;
  include 'submit/DBFunctions.php';
?>

<style>
  input, select {
    width: 100%;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
  }

  input[type=textarea]{
    width: 100%;
    height: 100px;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
  }

  input[type=submit] {
    width: 100%;
    background-color: #4CAF50;
    color: white;
    padding: 14px 20px;
    margin: 8px 0;
    border: none;
    border-radius: 4px;
    cursor: pointer;
  }

  input[type=submit]:hover {
    background-color: #45a049;
  }

  div {
    width:50%;
    border-radius: 5px;
    background-color: #f2f2f2;
    padding: 20px;
}
</style>

<div>
  <h2>Image Upload file</h2>

  <form action="submit/uploadImg.php" method="post" enctype="multipart/form-data">
    <label>Title</label>
    <input name="imgTitle" type="text" required ></input>
    </br>

    <label>Gallery</label>
    <input name="imgGallery" type="text" required></input>
    </br>

    <label>file (use jpg or png)</label>
    <input name="fileUpload" id="fileUpload" type="file" required ></input>
    </br>

    <label>Tags (use commas between)</label>
    <input name="sermonTags" type="text" ></input>
    </br>

    <input type="submit" value="upload"></input>
  </form>
</div>
</html>
