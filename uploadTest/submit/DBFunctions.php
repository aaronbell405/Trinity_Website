<?php

//test databace
//localhost db info
///*
$serverName = "localhost";
$userName = "testUpload";
$password = "password";
$DBname = "trinity_db";
//*/
/*
//QUB server
$serverName = "abell536.students.cs.qub.ac.uk";
$userName = "abell536";
$password = "3rpzsdlrtm2tmsjm";
$DBname = "abell536";
*/

function test_input($data){
  $data = trim($data);
  $data = stripcslashes($data);
  $data = htmlspecialchars($data);
  return $data;
}

function getConnection(){
  $conn = mysqli_connect($GLOBALS['serverName'], $GLOBALS['userName'], $GLOBALS['password'], $GLOBALS['DBname']);
  // Check connection
  if (!$conn) {
    echo "conection fail</br>";
    echo "server name:". $GLOBALS['serverName']. "</br>";
    echo "user: ". $GLOBALS['userName']. "</br>";
    echo "DB name". $GLOBALS['DBname']. "</br>";
    die("Connection failed: " . mysqli_connect_error());
    return NULL;
  }

  return $conn;
}

function tableExists($tableName){
  $conn = getConnection();
  $returnValue = 0;
  $val = mysqli_query($conn, 'select 1 from `'. $tableName. '` LIMIT 1');
  if($val !== FALSE){
   //table Exixts
   $returnValue = 1;
  }
  else{
    //no table found
    $returnValue = 0;
  }
  //echo "return value". $returnValue;
  return $returnValue;
}

Function createAnnousmentsTable(){
  $conn = getConnection();
  $sql = "CREATE TABLE `". $GLOBALS['DBname']. "`.`announcements_new` (
    `num` INT NOT NULL AUTO_INCREMENT ,
    `PostDate` DATETIME NOT NULL ,
    `Subject` TEXT NOT NULL ,
    `PostText` TEXT NOT NULL ,
    `Location` TEXT ,
    `Speaker` TEXT ,
    `StartDate` DATE NOT NULL ,
    `EndDate` DATE NOT NULL ,
    PRIMARY KEY (`num`)
    ) ENGINE = InnoDB;
  ";
  if (mysqli_query($conn, $sql)) {
    echo "table created </br>";
  } else {
    echo "Error: " . $sql . "<br>" . mysqli_error($conn);
  }
  mysqli_close($conn);
}

Function createsermonRecTable(){
  echo "create connection";
  $conn = getConnection();

  echo "make statment";
  $sql = "CREATE TABLE `". $GLOBALS['DBname']. "`.`sermonRec` (
    `num` INT NOT NULL AUTO_INCREMENT ,
    `sermonDate` DATE NOT NULL ,
    `sermonTime` TEXT NOT NULL ,
    `sermonFilePath` TEXT NOT NULL ,
    `sermonTitle` TEXT NOT NULL ,
    `sermonSpeaker` TEXT NOT NULL ,
    `sermonReading` TEXT NOT NULL ,
    `sermonSeries` TEXT,
    `sermonSeriesNo` INT ,
    `sermonTags` TEXT,
    PRIMARY KEY (`num`)
    ) ENGINE = InnoDB;
  ";

  if (mysqli_query($conn, $sql)) {
    echo "table created </br>";
  } else {
    echo "Error: " . $sql . "<br>" . mysqli_error($conn);
  }

  mysqli_close($conn);
}
Function createImgGallery(){
  $conn = getConnection();
  $sql = "CREATE TABLE `". $GLOBALS['DBname']. "`.`imgGallery` (
    `num` INT NOT NULL AUTO_INCREMENT ,
    `imgTitle` TEXT NOT NULL ,
    `imgGallery` TEXT NOT NULL,
    `ImgFilePath` TEXT NOT NULL ,
    `imgTags` TEXT ,
    PRIMARY KEY (`num`)
    ) ENGINE = InnoDB;
  ";
  if (mysqli_query($conn, $sql)) {
    echo "table created </br>";
  } else {
    echo "Error: " . $sql . "<br>" . mysqli_error($conn);
  }
  mysqli_close($conn);
}

?>
