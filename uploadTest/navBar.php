
<head>
<style>
  ul {
      list-style-type: none;
      margin: 0;
      padding: 0;
      overflow: hidden;
      background-color: #333;
  }

  li {
      float: left;
  }

  li a {
      display: block;
      color: white;
      text-align: center;
      padding: 14px 16px;
      text-decoration: none;
  }

  li a:hover {
      background-color: #111;
  }
</style>
</head>
<body>

<ul>
  <li><a href="index.php">home</a></li>
  <li><a href="announcements.php">Announcements</a></li>
  <li><a href="sermons.php">Sermons</a></li>
  <li><a href="announcementsForm.php">Create Announcements</a></li>
  <li><a href="uploadSermonForm.php">Upload Sermon</a></il>
  <li><a href="uploadImageForm.php"> Upload Image</a></il>
  <!--<li><a href="#contact">Contact</a></li>
  <li><a href="#about">About</a></li>-->
</ul>
