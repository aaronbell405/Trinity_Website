<html>
  <?php
    include 'navBar.php' ;
    include 'submit/DBFunctions.php';
  ?>
  <style>
    .announcement{
      margin: 10;
      width: 100%;
      background-color: rgb(124, 172, 235);
    }

    .metaInfo{
      display: inline-flex;

    }

    .metaItem{
      margin-right: 30;
    }
  </style>

  <h2>announcements</h2>

  <?php
    //Connect to the server
    $conn = getConnection();

    /*query will need to be changed to reflect the lenth of artacals and the
    relavince of aticals.
    */
    $query = "SELECT * FROM `announcements_new`";

    $result = mysqli_query($conn, $query);

    while($row = mysqli_fetch_array($result)){
      echo "<div class='announcement'>";

      echo "<h3 class='Subject'>";
      echo $row['Subject'];
      echo "</h3>";

      echo "<p class='postText'>";
      echo $row['PostText'];
      echo "</p>";

      echo "<div class='metaInfo'>";
      echo "<p class='Location metaItem'> Place: ";
      echo $row['Location'];
      echo "</p>";

      echo "<p class='Speaker metaItem'> Speaker: ";
      echo $row['Speaker'];
      echo "</p>";

      echo "<p class='StartDate metaItem'> Start Date: ";
      echo $row['StartDate'];
      echo "</p>";

      echo "<p class='EndDate metaItem'> End Date: ";
      echo $row['EndDate'];
      echo "</p>";

      echo "</div>";
      echo "</div>";
    }
  ?>

</html>
